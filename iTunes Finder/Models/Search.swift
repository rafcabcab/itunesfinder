//
//  Results.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 11/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import Foundation

struct Search: Decodable {
    var resultCount: Int
    var results: [Artist]
    
    init() {
        resultCount = 0
        results = []
    }
}
