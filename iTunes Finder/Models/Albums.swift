//
//  Album.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 11/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import Foundation
import UIKit

class Albums: Decodable {
    let resultCount: Int
    var results: [Album]
    
    init() {
        resultCount = 0
        results = []
    }
}

class Album: Decodable {
    let ImageURL: String
    let Title: String
    let Year: String
    var Image: UIImage!

    init(imageURL: String, title: String, year: String) {
        ImageURL = imageURL
        Title = title
        Year = year
    }
    
    init (){
        ImageURL = ""
        Title = ""
        Year = ""
    }
    
    enum AlbumCodingKeys : String, CodingKey {
        case WrapperType = "wrapperType"
        case Image = "artworkUrl100"
        case Title = "collectionName"
        case ReleaseDate = "releaseDate"
    }
    
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: AlbumCodingKeys.self)
        let type: String = try container.decode(String.self, forKey: .WrapperType)
        
        if type == "collection"
        {
            let image: String = try container.decode(String.self, forKey: .Image)
            let title: String = try container.decode(String.self, forKey: .Title)
            let releaseDate: String = try container.decode(String.self, forKey: .ReleaseDate)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let calendar = Calendar.current
            let year = calendar.component(.year, from: dateFormatter.date(from:releaseDate)!).description
            
            self.init(imageURL: image, title: title, year: year)
        }
        else{
            self.init()
        }
        
    }
}
