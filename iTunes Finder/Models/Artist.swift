//
//  Artist.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 11/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import Foundation

class Artist: Decodable {
    let Name: String
    let Id: Int
    let Genre: String
    var Albums: [Album]

    init(name: String, id: Int, genre: String) {
        self.Name = name
        self.Id = id
        self.Genre = genre
        Albums = []
    }
    
    enum ArtistCodingKeys : String, CodingKey {
        case Name = "artistName"
        case Id = "artistId"
        case Genre = "primaryGenreName"
    }
    
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ArtistCodingKeys.self)
        let name: String = try container.decode(String.self, forKey: .Name)
        let id: Int = try container.decode(Int.self, forKey: .Id)
        let genre: String = try container.decode(String.self, forKey: .Genre)
        
        self.init(name: name, id: id, genre: genre)
    }
}
