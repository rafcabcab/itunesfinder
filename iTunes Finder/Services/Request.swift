//
//  Finder.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 11/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import Foundation
import UIKit

class Finder: NSObject, URLSessionDelegate, URLSessionDataDelegate {
    
    var session: URLSession = URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: nil)
    
    static let Instance = Finder()
    
    private override init()
    {
        super.init()
        session = URLSession(configuration: .ephemeral, delegate: self, delegateQueue: nil)
    }
    
    private func newRequest(withURL url: String, method: String = "GET") -> URLRequest {
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }
    
    func searchArtists(name: String, completion: @escaping (_ search: Search, _ error: Error?) -> Void)
    {
        let formattedName = name.replacingOccurrences(of: " ", with: "+")
        
        let url = "https://itunes.apple.com/search?term=\(formattedName)&media=music&entity=musicArtist&limit=15"
        
        request(url: url) { (data, error) in
            
            if error != nil {
                completion(Search(), error)
                return
            }
            
            guard let search = try? JSONDecoder().decode(Search.self, from: data!) else {
                completion(Search(), nil)
                return
            }
            
            completion(search, nil)
        }
    }
    
    func getAlbumsByArtistId(artistId: Int, completion: @escaping (_ albums: [Album], _ error: Error?) -> Void)
    {
        let url = "https://itunes.apple.com/lookup?id=\(artistId)&entity=album&limit=200"
        
        request(url: url) { (data, error) in
            
            if error != nil {
                completion([], error)
                return
            }
            
            guard let albums = try? JSONDecoder().decode(Albums.self, from: data!) else {
                completion([], nil)
                return
            }

            albums.results.remove(at: 0)
            completion(albums.results, nil)

        }
    }
    
    func request(url: String, completion: @escaping (Data?, Error?) -> Void){
        
        let request = newRequest(withURL: url)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            if error != nil {
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, nil)
                return
            }
            
            if httpResponse.statusCode == 200 {
                if let data = data {
                    completion(data, nil)
                } else {
                    completion(nil, nil)
                    return
                }
            }
            
        }
        task.resume()
    }
    
}
