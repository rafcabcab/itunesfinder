//
//  GetCoverOperation.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 30/07/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import Foundation
import UIKit

class GetCoverOperation : Operation {
    
    var album: Album
    
    init(album: Album) {
        self.album = album
        super.init()
    }
    
    override func main(){

        if self.isCancelled {
            return
        }
        
        guard let url = URL(string: album.ImageURL) else {
            return
        }
        
        let data = try! Data(contentsOf: url)

        if self.isCancelled {
            return
        }
        
        if data.count > 0 {
            album.Image = UIImage(data: data)
        }

    }
    
}
