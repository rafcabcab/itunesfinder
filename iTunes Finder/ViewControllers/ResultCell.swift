//
//  ResultCell.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 14/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import UIKit

class ResultCell: UITableViewCell {
    
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var lblDiscography: UILabel!
    @IBOutlet weak var imgAlbumA: UIImageView!
    @IBOutlet weak var imgAlbumB: UIImageView!
    
}
