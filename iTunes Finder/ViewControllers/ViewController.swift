//
//  ViewController.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 11/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbSearchBar: UITextField!
    @IBOutlet weak var tvResults: UITableView!
    
    var downloadsInProgress = [IndexPath: Operation]()
    let downloadQueue = OperationQueue()
    
    var task: DispatchWorkItem!
    var search: Search = Search()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvResults.dataSource = self
        tvResults.delegate = self
        tvResults.register(UINib(nibName: "ResultCell", bundle: nil), forCellReuseIdentifier: "ResultCell")
        configBorder(layer: tbSearchBar.layer, width: 1.0, radius: 15.0)
    }
    
    func configBorder(layer: CALayer, width: Float = 0.0, radius: Float = 0.0, color: UIColor = UIColor.black) {
        layer.borderWidth = CGFloat.init(width)
        layer.cornerRadius = CGFloat.init(radius)
        layer.borderColor = color.cgColor
        layer.masksToBounds = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return search.resultCount
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        let cell = tableView.dequeueReusableCell(withIdentifier: "artistCell", for: indexPath)
            as! ResultCell
        
        configBorder(layer: cell.layer, width: 1.0, radius: 5.0)
        
        let artist = search.results[indexPath.row]
        cell.lblArtistName.text = artist.Name
        cell.lblGenre.text = artist.Genre
        
        if artist.Albums.count > 0
        {
            if artist.Albums[0].Image != nil {
                cell.imgAlbumA.image = artist.Albums[0].Image
                cell.lblDiscography.isHidden = false
                cell.imgAlbumA.isHidden = false
            } else {
                downloadCover(artist.Albums[0], atIndexPath: indexPath)
            }
        }
        
        if artist.Albums.count > 1
        {
            if artist.Albums[1].Image != nil {
                cell.imgAlbumB.image = artist.Albums[1].Image
                cell.imgAlbumB.isHidden = false
            }else {
                downloadCover(artist.Albums[1], atIndexPath: indexPath)
            }
        }
        
        return cell
    }
    
    @IBAction func tbSearchBar_EditDidEnd(_ sender: Any) {
        
        search.results.removeAll()
        search.resultCount = 0
        tvResults.reloadData()
        
        Finder.Instance.searchArtists(name: self.tbSearchBar.text!) { (search, error) in
            if error == nil {
                self.search = search
                
                DispatchQueue.main.async {
                    self.tvResults.reloadData()
                }

                for artist in search.results {
                    Finder.Instance.getAlbumsByArtistId(artistId: artist.Id) { (albums, error) in
                        if error == nil {
                            
                            if albums.count > 0 {
                                artist.Albums = albums

                                DispatchQueue.main.async {
                                    self.tvResults.reloadData()
                                }
                            }
                            
                        } else {
                            print("\(error!.localizedDescription)")
                        }
                    }
                }
                
            } else {
                print("\(error!.localizedDescription)")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let item = sender as? UITableViewCell
        let indexPath = tvResults.indexPath(for: item!)
        let detailVC = segue.destination as! ArtistViewController
        detailVC.setArtist(artist: search.results[(indexPath?.row)!])
    }
    
    func downloadCover(_ album: Album, atIndexPath indexPath: IndexPath) {
        if let _ = downloadsInProgress[indexPath] {
            return
        }
        
        let operation = GetCoverOperation(album: album)
        
        operation.completionBlock = {
            if operation.isCancelled {
                return
            }
            
            DispatchQueue.main.async {
                self.downloadsInProgress.removeValue(forKey: indexPath)
                self.tvResults.reloadRows(at: [indexPath], with: .automatic)
            }
        }
        
        downloadsInProgress[indexPath] = operation
        downloadQueue.addOperation(operation)
    }
}
