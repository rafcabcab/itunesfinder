//
//  ArtistViewcontroller.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 15/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import Foundation
import UIKit

class ArtistViewController: UICollectionViewController {
    
    @IBOutlet var cvAlbums: UICollectionView!
    
    
    var albums: [Album] = []
    
    var downloadsInProgress = [IndexPath: Operation]()
    let downloadQueue = OperationQueue()
    
    func setArtist(artist: Artist){
        albums = artist.Albums.sorted(by: {$0.Year < $1.Year})
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "albumCellReuse"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! AlbumCell
        
        let album = albums[indexPath.row]
        
        cell.lblTitle.text = "\(album.Year)\n\(album.Title)"
        
        if album.Image != nil {
            cell.imgCover.image = album.Image
        } else {
            downloadCover(album, atIndexPath: indexPath)
        }
        
        return cell
    }
    
    func downloadCover(_ album: Album, atIndexPath indexPath: IndexPath) {
        if let _ = downloadsInProgress[indexPath] {
            return
        }
        
        let operation = GetCoverOperation(album: album)
        
        operation.completionBlock = {
            if operation.isCancelled {
                return
            }
            
            DispatchQueue.main.async {
                self.downloadsInProgress.removeValue(forKey: indexPath)
                self.cvAlbums.reloadItems(at: [indexPath])
            }
        }
        
        downloadsInProgress[indexPath] = operation
        downloadQueue.addOperation(operation)
    }
}
