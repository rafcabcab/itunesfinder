//
//  AlbumCell.swift
//  iTunes Finder
//
//  Created by CodigoNeo on 15/06/2019.
//  Copyright © 2019 rafcab. All rights reserved.
//

import Foundation
import UIKit

class AlbumCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
}
